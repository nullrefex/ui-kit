/* eslint-env browser */
(function () {
    'use strict';

    var sliders = document.getElementsByClassName('ui-kit-slider');
    var length = sliders.length;
    for (var i = 0; i < length; i++) {
        var slider = sliders[i];
        var ignore = slider.getAttribute('data-ignore');
        if (ignore) {
            continue;
        }
        var type = slider.getAttribute('data-type');
        type = (type) ? type : 'one';
        var step = slider.getAttribute('data-step');
        step = (step) ? parseFloat(step) : 1;
        var min = slider.getAttribute('data-min');
        min = (min) ? parseFloat(min) : 0;
        var max = slider.getAttribute('data-max');
        max = (max) ? parseFloat(max) : 100;
        var start = slider.getAttribute('data-start');
        if (type == 'one') {
            start = (start) ? parseFloat(start) : 0;
            noUiSlider.create(slider, {
                start: start,
                step: step,
                range: {
                    'min': min,
                    'max': max
                }
            });
        }
        if (type == 'two') {
            if (start) {
                var split = start.split(',');
                start [split[0], split[1]];
            } else {
                start = [min, max];
            }
            noUiSlider.create(slider, {
                start: start,
                step: step,
                connect: true,
                range: {
                    'min': min,
                    'max': max
                }
            });
        }
    }
})();